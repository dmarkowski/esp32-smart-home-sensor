# ESP32-smart-home-sensor
## Table of content
* [Intro](#smart-home-sensor-based-on-esp32)
* [Table of content](#table-of-content)
* [Features](#features)
* [Logic](#logic)
* [Hardware](#hardware)
    + [Components](#components)
    + [NodeMCU-32S details](#nodemcu-32s-details)
* [Custom PCB](#custom-pcb)
* [Todo](#todo)

## Intro
ESP32-smart-home-sensor is a smart home sensor based on ESP-32 chip. The sensor can be placed in every room and connected to WiFi and 4.5V - 7V power suplier. It can measure temperature, humidity and light level and can control up to 2 AC/DC devices via relays. 

## Features
1. 4.5-7V powered
2. Power voltage measurement
3. Temperature measurements via DHT22 or DS18B20 sensor
4. Wifi connection and ping check
5. Light level measurement
6. Loud alarm buzzer
7. Led control
8. Up to 2 relay control
9. Periodic measurements (15 min)
10. Low power consumption - due to deep sleep between measurements


## Logic
![Smart home sensor diagram](resources/esp32-smart-home-sensor-diagram.png)


## Hardware
### Components
1. NodeMCU-32S
2. Temperature sensor DHT22 or DS18B20
3. Photoresistor GL55 10-30K Ohm
4. 5V buzzer
5. 2x AC/DC relays with 3.3V coil
6. 4.5-7V power suplier
7. LCD display hd44780


### NodeMCU-32S details
![NodeMCU-32S board](resources/esp32s-nodemcu-board.png)
![NodeMCU-32S pins definition](resources/esp32s-nodemcu-pins.png)


## Custom PCB
![Schema](resources/esp-sensor-sch.png)
![Board](resources/esp-sensor-brd.png)

## Todo
1. Send data to Home Assistant host via MQTT protocol
2. Integration with air polution sensor
3. Integration with door contact sensor
4. Integration with PIR-based motion detection sensor
