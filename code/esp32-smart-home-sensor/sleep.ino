#define DEEP_SLEEP_TIME 15

void SLEEP_GoDeepSleep() {
  int timeUs = DEEP_SLEEP_TIME * 60 * 1000000;
  Serial.printf("Sleep for: %ius\n", timeUs);
  esp_sleep_enable_timer_wakeup(timeUs);
  esp_deep_sleep_start();
}
