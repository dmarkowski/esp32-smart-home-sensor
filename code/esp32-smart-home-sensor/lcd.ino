#if LCD_ON

#include <Wire.h>
#include <hd44780.h>
#include <hd44780ioClass/hd44780_I2Cexp.h>

hd44780_I2Cexp lcd;
const int LCD_COLS = 16;
const int LCD_ROWS = 2;

void LCD_Init() {
  lcd.begin(LCD_COLS, LCD_ROWS);
  lcd.clear();
  lcd.backlight();
  lcd.setCursor(0, 0);
}

void LCD_Display(wl_status_t wifiStatus,
                 bool pingResult, float pingTime,
                 float hum, float temp,
                 bool isBright, float volt) {
  lcd.setCursor(0, 0);
  char line0[20];
  char line1[20];

  int wifiResult = 0;
  if (wifiStatus==WL_CONNECTED && pingResult) {
    wifiResult = (int)pingTime;
    if (wifiResult > 999)
      wifiResult = 999;
  }
  
  sprintf(line0, "W%03ims V%1.1fV", wifiResult, volt);
  lcd.print(line0);

  float tempResult = 0;
  if (!isnan(temp)) {
    tempResult = temp;
    if(tempResult>99.9)
      tempResult = 99.9;
  }
  float humResult = 0;
  if (!isnan(hum)) {
    humResult = hum;
    if(humResult>99.9)
      humResult = 99.9;
  }
  
  lcd.setCursor(0, 1);
  sprintf(line1, "T%02.1fC H%02.1f%% L%i", tempResult, humResult, isBright);
  lcd.print(line1);
}

void LCD_Stop() {
  lcd.noBacklight();
}

#endif
