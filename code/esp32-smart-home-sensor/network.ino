#include <ESP32Ping.h>
#define GOOGLE_HOST "www.google.com"

wl_status_t NET_InitWifi() {
  WiFi.begin(WIFI_SSID, WIFI_PASSWD);
  for ( int i = 0; i < 100 || WiFi.status() != WL_CONNECTED; i++) {
    delay(100);
  }
  return WiFi.status();
}

void NET_SendWifiStatus(wl_status_t status) {
  if (status == WL_CONNECTED)
    Serial.printf("Wifi status: %i - OK\n", status);
  else
    Serial.printf("Wifi status: %i - ERROR\n", status);
}

bool NET_CheckPing(float *avgTime) {
  bool pingResult = Ping.ping(GOOGLE_HOST, 10);

  if (pingResult)
    *avgTime = Ping.averageTime();

  return pingResult;
}

void NET_SendPing(bool result, float time) {
  if (result) {
    Serial.printf("Ping: RTT = %.2fms\n", time);
  } else {
    Serial.println("Ping: RTT = n/a - ERROR");
  }
}
