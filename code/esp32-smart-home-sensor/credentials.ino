// Replace with your actual SSID and password:
#define WIFI_SSID "Your SSID here"
#define WIFI_PASSWD "WLAN AP password here"

// Useful git commands:
// git update-index --assume-unchanged code/esp32-smart-home-sensor/credentials.ino
// git update-index --no-assume-unchanged code/esp32-smart-home-sensor/credentials.ino
