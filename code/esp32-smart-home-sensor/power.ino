#define POWER_SENSOR_PIN 32
#define VOLTAGE_DEVIDER_R1 1000
#define VOLTAGE_DEVIDER_R2 2200
#define POWER_SENSOR_SAMPLE_N 10
#define POWER_SENSOR_BIAS 0.5f

float PWR_ReadVoltage() {
  int raw = analogRead(POWER_SENSOR_PIN);
  float vAdc = PWR_ReadVoltageAdc();
  float r1 = (float)VOLTAGE_DEVIDER_R1;
  float r2 = (float)VOLTAGE_DEVIDER_R2;
  float pwrVolt = (((r1 + r2) * vAdc) / r1) + POWER_SENSOR_BIAS;
  return pwrVolt;
}

float PWR_ReadVoltageAdc() {
  int sum = 0;
  for (int i = 0; i < POWER_SENSOR_SAMPLE_N; i++) {
    sum += analogRead(POWER_SENSOR_PIN);
  }
  float avg = ((float)sum) / POWER_SENSOR_SAMPLE_N;  

  return ((float)avg * 3.3 ) / (4095.0);
}

void PWR_SendVoltage(float volt) {
  Serial.printf("Power voltage: %.2fV\n",  volt);
}
