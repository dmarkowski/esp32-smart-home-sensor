#if DS18_ON

#include <OneWire.h>
#include <DallasTemperature.h>

#define DS18B20_PIN 23

OneWire oneWire(DS18B20_PIN);
DallasTemperature sensors(&oneWire);

void DS18_Init() {
  sensors.begin();
}

float DS18_ReadTemperature() {
  sensors.requestTemperatures();
  float temp = sensors.getTempCByIndex(0);
}

void DS18_SendTemperature(float temp) {
  if (temp < -126)
    Serial.println("Temperature: n/a - ERROR");
  else {
    Serial.printf("Temperature: %.2f°C\n", temp);
  }
}

#endif
