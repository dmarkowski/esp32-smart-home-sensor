#include <DHT.h>
#include <WiFi.h>

#define VOLTAGE_LOW_LEVEL 4.0f
#define DS18_ON 0
#define DHT_ON 1
#define LCD_ON 1

void setup()
{

  Serial.begin(115200);

  BUZ_Init();
  LED_Init();
  LED_Blink(2);

  wl_status_t wifiStatus = NET_InitWifi();
  NET_SendWifiStatus(wifiStatus);

  float pingTime = 0;
  bool pingResult = 0;
  if (wifiStatus == WL_CONNECTED) {
    pingResult = NET_CheckPing(&pingTime);
    NET_SendPing(pingResult, pingTime);
  }

  bool isBright = LIGHT_IsBright();
  LIGHT_SendIsBright(isBright);

  float volt = PWR_ReadVoltage();
  PWR_SendVoltage(volt);

  float temp = 0;
  float hum = 0;

#if DHT_ON
  DHT_Init();

  hum = DHT_ReadHumidity();
  DHT_SendHumidity(hum);

  temp = DHT_ReadTemperature();
  DHT_SendTemperature(temp);
#endif

#if DS18_ON
  DS18_Init();

  temp = DS18_ReadTemperature();
  DS18_SendTemperature(temp);
#endif

#if LCD_ON
  LCD_Init();
  LCD_Display(wifiStatus, pingResult, pingTime,
              hum, temp, isBright, volt);
  delay(10000);
  LCD_Stop();
#endif

  LED_Blink(2);

  if (wifiStatus != WL_CONNECTED || volt < VOLTAGE_LOW_LEVEL) {
    BUZ_Pulse(10);
  }

  SLEEP_GoDeepSleep();
}

void loop() {
}
