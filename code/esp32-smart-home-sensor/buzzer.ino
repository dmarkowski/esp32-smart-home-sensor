#define BUZZER_PIN 35
#define BUZZER_PERIOD 0.5f

int buzzerCurrentState = LOW;

void BUZ_Init() {
  pinMode(BUZZER_PIN, OUTPUT);
  BUZ_Set(buzzerCurrentState);
}

void BUZ_Set(int state) {
  digitalWrite(BUZZER_PIN, state);
  buzzerCurrentState = state;
}

void BUZ_Toogle() {
  BUZ_Set(!buzzerCurrentState);
}

void BUZ_Pulse(int number) {
  int toogleN = number * 2;
  int halfPeriod = (int)(BUZZER_PERIOD * 500.0);
  for (int i = 0; i < toogleN; i++) {
    BUZ_Toogle();
    delay(halfPeriod);
  }
}
