#define LIGHT_SENSOR_PIN 34
#define LIGHT_SENSOR_SAMPLE_N 10
#define BRIGHT_THRESHOLD 100

bool LIGHT_IsBright() {
  int value = LIGHT_ReadLighAdc();
  return value > BRIGHT_THRESHOLD;
}

float LIGHT_ReadLighAdc() {
  int sum = 0;
  for (int i = 0; i < LIGHT_SENSOR_SAMPLE_N; i++) {
    sum += analogRead(LIGHT_SENSOR_PIN);
  }
  return ((float)sum) / LIGHT_SENSOR_SAMPLE_N;
}

void LIGHT_SendIsBright(bool isBright) {
  Serial.printf("Ligh sensor: %i\n", isBright);
}
