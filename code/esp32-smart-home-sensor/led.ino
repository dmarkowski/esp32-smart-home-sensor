#define LED_PIN 2
#define BLINK_PERIOD 0.4f

int ledCurrentState = LOW;

void LED_Init() {
  pinMode(LED_PIN, OUTPUT);
  LED_Set(ledCurrentState);
}

void LED_Set(int state) {
  digitalWrite(LED_PIN, state);
  ledCurrentState = state;
}

void LED_Toogle() {
  LED_Set(!ledCurrentState);
}

void LED_Blink(int number) {
  int toogleN = number * 2;
  int halfPeriod = (int)(BLINK_PERIOD * 500.0);
  for (int i = 0; i < toogleN; i++) {
    LED_Toogle();
    delay(halfPeriod);
  }
}
