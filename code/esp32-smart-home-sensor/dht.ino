#if DHT_ON

#define DHT_PIN 23
#define DHT_TYPE DHT22

DHT dht(DHT_PIN, DHT_TYPE);

void DHT_Init() {
  dht.begin();
}

float DHT_ReadHumidity() {
  float h = dht.readHumidity();
  return h;
}

float DHT_ReadTemperature() {
  float t = dht.readTemperature();
  return t;
}

void DHT_SendTemperature(float temp) {
  if (isnan(temp))
    Serial.println("Temperature: n/a - ERROR");
  else {
    Serial.printf("Temperature: %.2f°C\n", temp);
  }
}

void DHT_SendHumidity(float hum) {
  if (isnan(hum))
    Serial.println("Humidity: n/a - ERROR");
  else {
    Serial.printf("Humidity: %.2f%%\n", hum);
  }
}

#endif
